﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayePrefs : MonoBehaviour
{
    public string nombre;
    public float record;
    private string recordPrefabName = "Record";
    private string namePrefabName = "Jugador";

    private void Awake()
    {
        LoadData();
    }

    public void Boton()
    {
        nombre = GameObject.Find("Controlador Partida").GetComponent<ControladorMenu>().nombre;
        record = GameObject.Find("Controlador Partida").GetComponent<ControladorJuego>().record;
    }

    private void OnDestroy()
    {
        SaveData();
    }

    private void SaveData()
    {
        PlayerPrefs.SetFloat(recordPrefabName, 0.0f);
        PlayerPrefs.SetString(namePrefabName, nombre);
    }

    private void LoadData()
    {
        record = PlayerPrefs.GetFloat(recordPrefabName);
        nombre = PlayerPrefs.GetString(namePrefabName);
    }
}
