﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class ControladorMenu : MonoBehaviour
{
    public string nombre;
    public float record;
    Canvas canvas1;
    Canvas canvas2;
    private string jugadorPrefabName = "Jugador";

    private void Start()
    {
        canvas1 = GameObject.Find("Canvas").GetComponent<Canvas>();
        canvas2 = GameObject.Find("Canvas2").GetComponent<Canvas>();
        canvas1.enabled = true;
        canvas2.enabled = false;
    }

    private void Awake()
    {
        LoadData();
    }

    private void SaveData()
    {
        PlayerPrefs.SetString(jugadorPrefabName, nombre);
    }

    private void LoadData()
    {
        nombre = PlayerPrefs.GetString(jugadorPrefabName);
    }

    public void Empezar()
    {
        canvas1.enabled = false;
        canvas2.enabled = true;
    }

    public void Atras()
    {
        canvas1.enabled = true;
        canvas2.enabled = false;
    }

    public void Validar(string escena)
    {
        nombre = GameObject.Find("InputNombre").GetComponent<TMP_InputField>().text;
        if (string.IsNullOrEmpty(nombre))
        {
            Debug.Log("Ingrese un nombre por favor...");
        }
        else
        {
            Debug.Log("Bienvendo: " + nombre);
            SaveData();
            CargarEscena(escena);
        }
    }
    public void CargarEscena(string escena)
    {
        SceneManager.LoadScene(escena);
    }

    public void Salir()
    {
        Application.Quit();
        Debug.Log("Se ha cerrado el juego");
    }
}
